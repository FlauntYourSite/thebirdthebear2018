<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package TheBirdTheBear2018
 */

get_header(); ?>

	<div class="para-hero"><img src="<?php the_field( 'header_image', get_option('page_for_posts') ); ?>"></div>  

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<header class="entry-header">
				<?php get_template_part( 'inc/animated-title' ); ?>
			</header><!-- .entry-header -->

			<section class="error-404 not-found">

				<div class="page-content">

					<div class="usa-four-o-four"><?php require ( 'images/usa.svg' ); ?></div>

					<div class="four-content">
						<p><?php esc_html_e( 'Get turned around? Maybe we can help! Try the search bar, or use the menu above.', 'thebirdthebear2018' ); ?></p>
						<?php get_search_form(); ?>
					</div>


				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
