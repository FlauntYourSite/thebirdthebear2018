<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package TheBirdTheBear2018
 */

?>

	</div><!-- #content -->

	<?php get_template_part( 'footer-meta' ); ?>

	
	<footer id="colophon" class="site-footer" role="contentinfo">

			<p class="copyright">&copy; <?php echo date('Y'); ?> The Bird &amp; The Bear.
				<?php if ( ! is_admin() ) : ?>
					<a href="/privacy-policy" class="privacy">Privacy Policy</a>
				<?php endif; ?>
			</p>
  			
			<p class="site-credit">
				<?php 

					if ( is_front_page() ){

						printf( esc_html__( 'Website Proudly Provided by: %s.', 'thebirdthebear2018' ), '<a href="https://flauntyoursite.com/">Flaunt Your Site</a>' ); 
				}else{
						printf( esc_html__( 'Website Proudly Provided by: %s.', 'thebirdthebear2018' ), '<a href="https://flauntyoursite.com/" rel="nofollow">Flaunt Your Site</a>' );

				}?>
			</p>

	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

<script>
	window.onload = function () {

		const players = Array.from(document.querySelectorAll('.plyr')).map(player => new Plyr(player, {
				controls: [
					'play-large', // The large play button in the center
					'progress', // The progress bar and scrubber for playback and buffering
					'mute', // Toggle mute
					'volume', // Volume control
					'captions', // Toggle captions
					'airplay', // Airplay (currently Safari only)
					'fullscreen' // Toggle fullscreen
				],
				muted:'true',
				showPosterOnEnd: 'true'
		},
		
			player.addEventListener('playing', event => {
						
				mySwiper.autoplay.stop()
				
			}),

			player.addEventListener('pause', event => {

				mySwiper.autoplay.start()
			
			}),
		));


		const mySwiper = new Swiper('.swiper-container', {
	    // Optional parameters
			speed: 2000,
			autoplay: true,
			effect: "fade",
			autoHeight: true,
			preloadImages: true,
			loadPrevNext: true,
			loadPrevNextAmount: 1,
			simulateTouch:false,
		// Navigation arrows
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
		})	
		
		mySwiper.on('slideChange', function () {
			
			jQuery('video.plyr').map(function(){this.pause()})

		})
	};
</script>

</body>
</html>