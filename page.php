<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package TheBirdTheBear2018
 */

get_header(); ?>
	<div class="para-hero"><img src="<?php the_field( 'header_image' ); ?>"></div>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<header class="entry-header">
				<?php get_template_part( 'inc/animated-title' ); ?>
			</header><!-- .entry-header -->
			
			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );


			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
