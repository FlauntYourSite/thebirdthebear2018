<div class="social">
    <?php tbtb_social( 'facebook',  'https://facebook.com/thebirdthebear', "Join us on Facebook"  ); ?>
    <?php tbtb_social( 'twitter',  'https://twitter.com/thebirdthebear', "Join us on Twitter" ); ?>
    <?php tbtb_social( 'instagram',  'https://www.instagram.com/thebirdthebear/', "Join us on Instagram" ); ?>
    <?php tbtb_social( 'pinterest',  'https://pinterest.com/carynloves/', "Join us on Pinterest" ); ?>
    <?php tbtb_social( 'rss',  '/feed/' , "Subscribe to our blog." ); ?>
</div>
