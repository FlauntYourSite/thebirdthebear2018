<?php
/**
 * The template for displaying home page.
 *
 * @package TheBirdTheBear2018
 */

get_header(); ?>

	<?php tbtb_slideshows( 'tbtb_header_slideshow', 'swiper-header home' ); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
		
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<section class="row fade">
					<div class="content-block" id="home-one">

						<div>	
							<?php require( 'images/bird-bear-logo.svg' ); ?>
						</div>

						<h2>
							<span class="title"><?php the_field( 'section_1_title' ); ?></span><br />
							<span class="subtitle"><?php the_field( 'section_1_subtitle' ); ?></span>
						</h2>
						<?php the_field( 'section_1_text' ); ?>

					</div>
				</section>

				<section class="row fade" id="horizontal-rain">						
						<div class="hello-friends"><?php require( 'images/hello-friends.svg' ); ?></div>
						<?php get_template_part( 'slideshow-rain-horizontal' ); ?>
				</section>

				<section class="row fade">
						<div class="content-block" id="home-three">

							<?php fsc_figure( 'section_3_image', 'large', 'content-block-image', '' ); ?>

							<div class="text vert-center">
								<h2>
									<span class="title"><?php the_field( 'section_3_title' ); ?></span><br />
									<span class="subtitle"><?php the_field( 'section_3_subtitle' ); ?></span>
								</h2>
								<?php the_field( 'section_3_text' ); ?>
							</div>

					</div>
					<?php include( 'images/four-branch.svg' ); ?>
					<?php include( 'images/four-branch.svg' ); ?>					
				</section>

				<section class="row fade" >
					<div class="section-wrap column-reverse">
						<div class="content-block" id="home-four">
							<div class="vert-center">
								<h2>
									<span class="title"><?php the_field( 'section_4_title' ); ?></span><br />
									<span class="subtitle"><?php the_field( 'section_4_subtitle' ); ?></span>
								</h2>
								<?php the_field( 'section_4_text' ); ?>
							</div>
						</div>
						<div class="home-four-frilly-circle"><?php include( 'images/frilly-circle.svg' ); ?></div>

						<div class="pink-circle-container">
							<div class="pink-circle">
								<div class="circle-sign-animation">
									<img class="right-arrow-bounce" src="/wp-content/themes/thebirdthebear2018/images/arrow-right-white.svg" />
									<a href="<?php the_field( 'section_4_button_link' ); ?>" class="button-white"><?php white_button( Inquire ); ?></a>
									<img class="left-arrow-bounce" src="/wp-content/themes/thebirdthebear2018/images/arrow-left-white.svg" />
								</div>
								<?php require( 'images/pink-circle.svg' ); ?>
							</div>
						</div>

					</div>
				</section>
				
				<section class="row fade" >
					<div class="content-block" id="home-five">
						<?php fsc_figure( 'section_5_image', 'large', 'content-block-image', '' ); ?>
						<div class="text">
							<h2>
								<span class="title"><?php the_field( 'section_5_title' ); ?></span><br />
								<span class="subtitle"><?php the_field( 'section_5_subtitle' ); ?></span>
							</h2>
							<?php the_field( 'section_5_text' ); ?>
						</div>
					</div>
					<?php include( 'images/four-branch.svg' ); ?>
					<?php include( 'images/four-branch.svg' ); ?>	
				</section>
				
				<section class="row fade">
					<div class="section-wrap column-reverse">
						<div class="content-block" id="home-six">
							<h2>
								<span class="title"><?php the_field( 'section_6_title' ); ?></span><br />
								<span class="subtitle"><?php the_field( 'section_6_subtitle' ); ?></span>
							</h2>
							<?php the_field( 'section_6_text' ); ?>
						</div>
						<div class="home-six-frilly-circle"><?php include( 'images/frilly-circle.svg' ); ?></div>
						<?php include( 'images/curved-arrow.svg' ); ?>


						<div class="pink-circle-container">
							<div class="pink-circle">
								<div class="circle-sign-animation">
									<img class="right-arrow-bounce" src="/wp-content/themes/thebirdthebear2018/images/arrow-right-white.svg" />
									<a href="<?php the_field( 'section_6_button_link' ); ?>" class="button-white"><?php white_button( Learn_More ); ?></a>
									<img class="left-arrow-bounce" src="/wp-content/themes/thebirdthebear2018/images/arrow-left-white.svg" />
								</div>
								<?php require( 'images/pink-circle.svg' ); ?>
							</div>
						</div>
					</div>	
				</section>
				
				<div class="texas"><?php include( 'images/texas.svg' ); ?></div>

			</article><!-- #post-<?php the_ID(); ?> -->



			<?php get_template_part( 'inc/insta-circle' ); ?>

			<?php get_template_part( 'inc/footer-insta-row' ); ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
