
/**
 * Set Branches
 * 
 */

const branchbla = function () {
    const fourBranch = document.querySelectorAll('.row .branch-group');

    for (var i = 0; i < fourBranch.length; i++) {
        if ((i % 2) === 0) {
            fourBranch[i].style.cssText = "position: absolute;transform: rotate(50deg);width: 250px;top: -50px;left: -165px;z-index: -1;";
        }

        if ((i % 2) !== 0) {
            fourBranch[i].style.cssText = "position: absolute;transform: rotate(-50deg);width: 250px;bottom: -160px;left: 39%;z-index: -1;";
        }
    }
};
branchbla()

/**
 * Polaroid Slide in right
 *
 */

const polaroidRight = function () {
    const images = document.querySelectorAll('.polaroid-right');
    const sliderVideos = document.querySelectorAll('.swiper-container .plyr');

    Array.prototype.map.call(images, function (image) {
        const tl = new TimelineMax();
        tl.to(image, 1.5, { right: '0%', marginRight: 0, left: 'auto', ease: Back.easeOut.config(1.1) })

        // 2. Curtain Scene
        const player = image.querySelector('.plyr')
        const section = image.closest("section")
        const scene = new ScrollMagic.Scene({ triggerElement: image, offset: -100 })
            .addTo(controller)
            .setTween(tl)

        const sceneTwo = new ScrollMagic.Scene({ triggerElement: section, offset: -100, duration: section.clientHeight + 100 })
            .addTo(controller)
            .on('enter', function () { player.play() })
            .on('leave', function () { player.pause() })

        const sceneThree = new ScrollMagic.Scene({ triggerElement: section, offset: -100 })
            .addTo(controller)
            .on('enter', function () {
                Array.prototype.map.call(sliderVideos, function (sliderVideo) {
                    sliderVideo.pause()
                })
            })
    });

};
if (window.screen.width > 768) {
    polaroidRight();
}

/**
 * Polaroid Slide in left
 *
 */

const polaroidLeft = function () {
    const images = document.querySelectorAll('.polaroid-left');

    Array.prototype.map.call(images, function (image) {
        var tl = new TimelineMax();
        tl.to(image, 1.5, { left: '0%', marginLeft: 0, ease: Back.easeOut.config(1.1) }, 0.5);

        // 2. Curtain Scene
        const player = image.querySelector('.plyr')
        const section = image.closest("section")
        var scene = new ScrollMagic.Scene({ triggerElement: image, offset: -100 })
            .addTo(controller)
            .setTween(tl)

        var sceneTwo = new ScrollMagic.Scene({ triggerElement: section, offset: -100, duration: section.clientHeight + 100 })
            .addTo(controller)
            .on('enter', function () { player.play() })
            .on('leave', function () { player.pause() })
    });
};
if (window.screen.width > 768) {
    polaroidLeft();
}

/**
 * Polaroid Slide below.
 *
 */

const polaroidDown = function () {

    const images = document.querySelectorAll('.polaroid');
    const row = document.querySelectorAll('.row');
    Array.prototype.map.call(images, function (image) {
        var imageHeight = image.clientHeight;

        var tl = new TimelineMax();
        tl.to(image, 1.5, { bottom: "-" + imageHeight + "px", height: imageHeight + "px", ease: Back.easeOut.config(1.1) }, 0.5)
            .to(row, 1.5, { marginBottom: ((imageHeight) + 40) + "px", ease: Back.easeOut.config(1.1) }, '-=1.5')

        // 2. Curtain Scene
        var scene = new ScrollMagic.Scene({ triggerElement: image, offset: -100 })
            .addTo(controller)
            .reverse(false)
            .setTween(tl)
    });
};

if (window.screen.width < 768) {
    polaroidDown();
}

