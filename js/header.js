/**
 * Menu pop in.
 *
 */

const menuDropIn = function () {
  const header = document.querySelector(".site-header")
  const ribbon = document.querySelector(".ribbon")

  const tl = new TimelineMax()
  tl
    .to(header, 1, {
      autoAlpha: 1,
      transform: "translateY(6em)",
      ease: Power4.easeIn
    })
    .to(
      ribbon,
      1.5,
      {
        transform: "translateY(18.5em)",
        ease: Elastic.easeOut.config(1.5, 0.3)
      },
      "+-0.25"
    )
}

menuDropIn()

/**
 *
 * Animated the H1 for each Page.
 *
 */

const animatedTitle = function (speed) {
  const title = document.querySelectorAll(".animated-title")
  const maskLeft = document.querySelectorAll("#mask-branch-left")
  const maskRight = document.querySelectorAll("#mask-branch-right")

  const tl = new TimelineMax()
  tl
    .to(title, speed, { autoAlpha: 1, ease: Power4.easeInOut })
    .from([maskLeft, maskRight], 1.5, { drawSVG: 1, ease: Power4.easeInOut }, "-=.75")
}

animatedTitle()

/**
 *
 * Mobile Menu animation
 *
 */

const mobileMenuToggle = function () {
  var menuToggle = document.querySelector(".menu-toggle")
  const ribbon = document.querySelector(".ribbon")
  const logoSmall = document.querySelector(".alt-logo")
  const menuContainer = document.querySelector(".menu-standard-container")
  const ul = document.querySelector(".main-navigation ul")
  const li = document.querySelectorAll(".main-navigation li")

  const tl = new TimelineMax({ paused: true })
  tl
    .to(ribbon, 1, {
      transform: "translateY(-18.5em)",
      ease: Elastic.easeIn.config(1, 1)
    })
    .to(logoSmall, 0.25, { autoAlpha: 1 })
    .to(menuContainer, 0.25, { autoAlpha: 1 })
    .staggerTo(li, 0.15, { right: 0 }, 0.25)

  menuToggle.addEventListener(
    "click",
    function (event) {
      if (!menuToggle.classList.contains("toggled")) {
        tl.play(0)
        menuToggle.classList.add("toggled")
        event.preventDefault()
      } else {
        tl.reverse(0)
        menuToggle.classList.remove("toggled")
        event.preventDefault()
      }
    },
    false
  )
}

if (window.screen.width < 768) {
  mobileMenuToggle()
}

/**
 *
 * Ribbonlogo hover rotate animation.
 *
 */

const rotateText = function () {
  const ribbonLogo = document.querySelector(".ribbon")
  const textCircle = document.querySelector("#text-circle")
  const rotateBase = 0

  const spin = new TimelineMax({ paused: true })

  spin.to(textCircle, 1, {
    transformOrigin: "50% 50%",
    rotation: -1440,
    ease: Elastic.easeInOut.config(1, 0.3)
  })

  ribbonLogo.addEventListener("mouseenter", function () {
    spin.play(rotateBase)
  })
}

rotateText()

/**
 * Photography Icon animation on hover
 *
 */

const photoMenuIcon = function () {
  const flash = document.querySelector("#flash")
  const photographyMenu = document.querySelector(".photography-menu")
  const div = document.createElement("div")

  photographyMenu.appendChild(div)
  div.innerHTML +=
    '<svg viewBox="0 0 48.51 50.78"><use xlink:href="#photography-menu-icon"></use></svg>'

  const tl = new TimelineMax({ paused: true, repeat: -1 })
  tl.to(flash, 0.5, { opacity: 1 })
  // .to(flash, .1, { opacity: 0, ease: Elastic.easeOut.config(1, 0.3) });

  photographyMenu.addEventListener("mouseenter", function () {
    tl.play(0)
  })
  photographyMenu.addEventListener("mouseleave", function () {
    tl.reverse(0)
  })
}
photoMenuIcon()

/**
 * Films icon animation on hover.
 *
 */

const filmMenuIcon = function () {
  const menuItem = document.querySelector(".films-menu")
  const div = document.createElement("div")

  menuItem.appendChild(div)
  div.innerHTML +=
    '<svg viewBox="0 0 48.36 42.28"><use xlink:href="#film-menu-icon"></use></svg>'

  const reelOne = document.querySelector("#reel-1")
  const reelTwo = document.querySelector("#reel-2")
  const filmsMenu = document.querySelector(".films-menu")
  const rollReel = new TimelineMax({ paused: true, repeat: -1 })
  rollReel.to([reelOne, reelTwo], 1, {
    transformOrigin: "50% 50%",
    rotation: 360,
    ease: Power0.easeNone
  })

  filmsMenu.addEventListener("mouseenter", function () {
    rollReel.play()
  })

  // hover out
  filmsMenu.addEventListener("mouseleave", function () {
    rollReel.pause()
  })
}
filmMenuIcon()

/**
 * Our Process icon animation on hover.
 *
 */

const processMenuIcon = function () {
  const processMenu = document.querySelector(".process-menu")
  const div = document.createElement("div")

  processMenu.appendChild(div)
  div.innerHTML +=
    '<svg viewBox="0 0 43.24 48"><use xlink:href="#heart-menu-icon"></use></svg>'

  const heartAnimate = new TimelineMax({ paused: true, repeat: -1 })

  heartAnimate.to(div, 0.8, {
    transformOrigin: "50% 25%",
    scaleX: 1.15,
    scaleY: 1.15,
    ease: Elastic.easeOut
  })

  processMenu.addEventListener("mouseover", function () {
    heartAnimate.play()
  })
  processMenu.addEventListener("mouseleave", function () {
    heartAnimate.pause()
  })
}

processMenuIcon()

/**
 * Journal icon animation on hover.
 *
 */

const journalMenuIcon = function () {
  const journalMenu = document.querySelector(".journal-menu")
  const div = document.createElement("div")

  journalMenu.appendChild(div)
  div.innerHTML +=
    '<svg viewBox="0 0 41.74 48"><use xlink:href="#journal-menu-icon"></use></svg>'

  const maskOne = document.querySelector("#line-one-mask")
  const maskTwo = document.querySelector("#line-two-mask")
  const maskThree = document.querySelector("#line-three-mask")
  const maskFour = document.querySelector("#line-four-mask")

  const journalAnimate = new TimelineMax({ paused: false })

  journalAnimate
    .from(maskOne, 0.3, { drawSVG: 1, ease: Power4.easeInOut })
    .from(maskTwo, 0.3, { drawSVG: 1, ease: Power4.easeInOut })
    .from(maskThree, 0.3, { drawSVG: 1, ease: Power4.easeInOut })
    .from(maskFour, 0.3, { drawSVG: 1, ease: Power4.easeInOut })

  journalMenu.addEventListener("mouseenter", function () {
    journalAnimate.play(0)
  })
}
journalMenuIcon()

/**
 * Contact icon animation on hover.
 *
 */

const contactMenuIcon = function () {
  const contactMenu = document.querySelector(".contact-menu")
  const div = document.createElement("div")

  contactMenu.appendChild(div)
  div.innerHTML +=
    '<svg viewBox="0 0 48 33.59"><use xlink:href="#contact-menu-icon"></use></svg>'

  const one = document.querySelector("#_1")
  const two = document.querySelector("#_2")
  const three = document.querySelector("#_3")
  const four = document.querySelector("#_4")
  const five = document.querySelector("#_5")
  const six = document.querySelector("#_6")
  const seven = document.querySelector("#_7")
  const eight = document.querySelector("#_8")

  const contactAnimate = new TimelineMax({ paused: true })

  contactAnimate
    .to([one, two, three, four, five, six, seven, eight], 0, { autoAlpha: 0 })
    .to(one, 0.125, { autoAlpha: 1 })
    .to(two, 0.125, { autoAlpha: 1 })
    .to(three, 0.125, { autoAlpha: 1 })
    .to(four, 0.125, { autoAlpha: 1 })
    .to(five, 0.125, { autoAlpha: 1 })
    .to(six, 0.125, { autoAlpha: 1 })
    .to(seven, 0.125, { autoAlpha: 1 })
    .to(eight, 0.125, { autoAlpha: 1 })

  contactMenu.addEventListener("mouseenter", function () {
    contactAnimate.play(0)
  })
}

contactMenuIcon()

/**
 *
 * Fade in effect for all Fades elements.
 *
 */

var fades = function () {
  var fades = document.querySelectorAll(".fade")

  Array.prototype.map.call(fades, function (fade) {
    fade.style.visibility = "hidden"
    // 2. Timeline
    var tl = new TimelineMax()
    tl
      .from(fade, 0.5, { scale: 0.95, y: 110 })
      .to(fade, 0.5, { autoAlpha: 1 }, "-=0.25")

    // 2. Curtain Scene
    var scene = new ScrollMagic.Scene({ triggerElement: fade, offset: -300 })
      .addTo(controller)
      .setTween(tl)
  })
}

fades()


/**
 *
 * Draws frilly branches
 *
 */

const branches = function () {
  var branchGroups = Array.prototype.slice.call(document.querySelectorAll(".branch-group"));
  if (branchGroups) {
    branchGroups.forEach(branchDraw);

    function branchDraw(group, index) {

      var masks = group.querySelectorAll(".mask");
      var paths = group.querySelectorAll(".mask-path");
      var layers = group.querySelectorAll(".layer")

      masks.forEach(function (mask, i) {
        var id = "group-" + index + "-mask-" + i;
        mask.id = id;
        layers[i].setAttribute("mask", "url(#" + id + ")");
      });

      // const tl = new TimelineMax() 
      var tl = new TimelineMax();

      tl
        .from(paths[0], 0.5, { drawSVG: 1, ease: Power2.easeOut }, .5)
        .from(paths[1], 0.75, { drawSVG: 1, ease: Power2.easeOut }, "-=.25")
        .from(paths[2], 0.75, { drawSVG: 1, ease: Power2.easeOut }, "-=.25")
        .from(paths[3], 0.75, { drawSVG: 1, ease: Power2.easeOut }, "-=.5");

      const scene = new ScrollMagic.Scene({ triggerElement: group, offset: -200 })
        .addTo(controller)
        .setTween(tl)
    }
  }
}
branches();



/**
 *
 * Draws Frilly circles
 *
 */

const frillyCircles = function () {
  var frillyCircleGroups = Array.prototype.slice.call(document.querySelectorAll(".frilly"));

  frillyCircleGroups.forEach(frillyCircleDraw);

  function frillyCircleDraw(group, index) {

    var masks = group.querySelectorAll(".frilly-mask");
    var paths = group.querySelectorAll(".mask-path");
    var layers = group.querySelectorAll(".layer")

    masks.forEach(function (mask, i) {
      var id = "frilly-" + index + "-mask-" + i;
      mask.id = id;
      layers[i].setAttribute("mask", "url(#" + id + ")");

    });

    // const tl = new TimelineMax() 
    var tl = new TimelineMax();

    tl.set(paths[0], { transformOrigin: "50% 50%", rotation: 180 })
      .from(paths[0], 1.5, { drawSVG: 1, ease: Power4.easeInOut })

    const scene = new ScrollMagic.Scene({ triggerElement: group, offset: -100 })
      .addTo(controller)
      .setTween(tl)
  }
}
frillyCircles();





/**
 *
 * Random Borders
 *
 */

var randomBorders = function () {
  var contentBlocks = document.querySelectorAll(".content-block")


  Array.prototype.forEach.call(contentBlocks, function (contentBlock) {
    var randomA = Math.floor(Math.random() * 8 * 4)
    var randomB = Math.floor(Math.random() * 8 * 4)
    var randomC = Math.floor(Math.random() * 8 * 4)
    var randomD = Math.floor(Math.random() * 8 * 4)

    contentBlock.style.borderImageOutset =
      randomA +
      "px" +
      " " +
      randomB +
      "px" +
      " " +
      randomC +
      "px" +
      " " +
      randomD +
      "px"
  })
}

randomBorders();

/**
 *
 * Fades in Hero container
 *
 */

const heroFadeIn = function () {
  const hero = document.querySelector(".swiper-container")
  if (hero) {
    const tl = new TimelineMax()
    tl.to(hero, 2, { autoAlpha: 1 }, 0.5)
  }
}
heroFadeIn()

/**
 *
 * Fades in ParaHero container
 *
 */

const paraHeroFadeIn = function () {
  const paraHero = document.querySelector(".para-hero")
  if (paraHero) {

    const tl = new TimelineMax()
    tl.to(paraHero, 2, { autoAlpha: 1 }, 0.5)
  }
}
paraHeroFadeIn()



/**
 * 
 * 
 */
const arrowThrob = function () {
  const arrowDown = document.querySelector(".arrow-down")

  if (arrowDown) {

    const tl = new TimelineMax({ repeat: -1, repeatDelay: 4 })

    tl
      .to(arrowDown, 0.2, { opacity: 0.5 })
      .to(arrowDown, 0.2, { opacity: 1 })
      .to(arrowDown, 0.2, { opacity: 0.5 })
      .to(arrowDown, 0.2, { opacity: 1 })
      .to(arrowDown, 0.2, { opacity: 0.5 })
      .to(arrowDown, 0.2, { opacity: 1 })
  }
}
window.onload = arrowThrob()


/**
 * 
 * 
 */
const usaAnimateRandom = function () {

  if (document.querySelector('.usa')) {

    const pink = document.querySelectorAll('#pink path');
    tl = new TimelineMax({ repeat: -1 });

    for (var i = 0; i < pink.length; i++) {
      tl.to(pink[i], .25, { fill: "#e6c5b6" })
    }
  }
}
usaAnimateRandom()


/**
 *
 * Draws circle and fades in Instagram icon.
 *
 */

const instaFooterLogo = function () {
  const instaIcon = document.querySelector("#ring-instagram")
  const maskCircle = document.querySelector("#ring-mask")
  const circle = document.querySelector("#circle")
  const p = document.querySelectorAll(".insta-circle p")

  if (instaIcon) {

    const tl = new TimelineMax()

    tl
      .from(circle, 1, { drawSVG: 1, ease: Power4.easeInOut })
      .fromTo(instaIcon, 0.5, { autoAlpha: 0 }, { autoAlpha: 1 }, "-=0.35")
      .fromTo(p, 0.5, { autoAlpha: 0 }, { autoAlpha: 1 }, "-=0.35")

    // 2. Curtain Scene
    const scene = new ScrollMagic.Scene({ triggerElement: circle, offset: -220 })
      .addTo(controller)
      .setTween(tl)

  }
}
instaFooterLogo()
