/**
 * 
 * Sets up the Modal Posts if larger than Mobile.
 * 
 */

if (window.screen.width > 768) {

  /**
   *
   * Ajax requst to the post, and onclick handler.
   *
   */


  jQuery(document).ready(function ($) {
    var $links = $("a.thumb")

    var $container = $(".modal-content")
    var cache = {} // An object in memory to store key:value pairs for pages we have already been to
    var selector = "article.post, article.instagram"

    // Create a reusable function to handle the AJAX stuff
    function ajaxContentIntoPage(href, key, selector) {
      $.ajax({
        type: "get",
        url: href,
        success: function (response) {
          // var selection = 'article.post'; // Update this
          var $content = $(response).find(selector)[0]

          // Add the content to the cache in case we come back to it
          cache[key] = $content

          // Add the content to the page
          $container.html($content)
        }
        // TODO: good idea to add logic in case of failed attempts.
      })
    }

    var initial;
    // What to do whenever any of our links is clicked.
    $(".post-container").on('click', 'a.thumb', function (event) {
      // Stop the page from navigating to the new page
      event.preventDefault()

      initial = $(this)
      // Get the hash we want to use for this link (can be any UNIQUE value like a post ID or slug).
      var hash = this.dataset.hash

      // If there is NOT content in the cache for this hash, fetch it.
      if (!cache.hash) {
        ajaxContentIntoPage(this.href, hash, selector)
      } else {
        // Or, we have a cached version, just use that.
        $container.html(cache.hash)
      }

      // Finally, add the hash to the URL in case someone shares page.
      window.location.hash = hash


      if (document.querySelector("link[rel=canonical]")) {
        const canonical = document.querySelector("link[rel=canonical]")
        document.head.removeChild(canonical)
      }
      var link = document.createElement("link")
      link.rel = "canonical"
      link.href = this.href
      var link = document.head.appendChild(link)


      const thumbs = document.querySelectorAll("a.thumb")
      const modalContainer = document.querySelector(".modal-container")
      const postModal = document.querySelector(".post-modal")
      const logoSmall = document.querySelector(".modal-logo")
      const postNav = document.querySelector(".post-nav-arrows")
      const body = document.querySelector("body")



      var tl = new TimelineMax({ paused: true })
      tl
        .to(modalContainer, 0.5, { autoAlpha: 1, display: "block" })
        .to(postModal, 0.5, { autoAlpha: 1 })
        .to([postNav, logoSmall], 0.5, { autoAlpha: 1 }, "-=.25")
        .call(function () { animatedTitle(.25) })
        .set(body, { overflowY: "hidden", position: "fixed" })

      tl.play(0)

    })

    $(".nav-next a").click(function (event) {
      // Stop the page from navigating to the new page
      event.preventDefault()
      initial.next().trigger('click')

    })

    $(".nav-previous a").click(function (event) {
      // Stop the page from navigating to the new page
      event.preventDefault()
      initial.prev().trigger('click')

    })

    // When the page first loads, check if there is a hash in the URL
    if (window.location.hash) {
      $('a.thumb[data-hash="' + window.location.hash.substr(1) + '"]').trigger('click');

    }

  })
  // Fully reference jQuery after this point.

  /**
   *
   * staggers the post thumbnails.
   *
   */

  const postThumbFade = function () {
    const postThumbs = document.querySelectorAll(".thumb img")

    postThumbs.forEach(function (postThumb) {
      postThumb.style.visibility = "hidden"
    })
    const tl = new TimelineMax()

    tl.staggerFrom(postThumbs, 0.5, { scale: 0.95, y: 110, autoAlpha: 0 }, 0.1)

    // 2. Curtain Scene
    const scene = new ScrollMagic.Scene({
      triggerElement: postThumbs,
      offset: -320
    })
      .addTo(controller)
      .setTween(tl)
    //   })
  }
  postThumbFade()


  /**
   *
   * Removes the modal and fades out the background wash.
   *
   *
   */

  var removeBackgroundWash = function (link) {
    var modalContainer = document.querySelector(".modal-container")
    var postModal = document.querySelector(".post-modal")
    var postNav = document.querySelector(".post-nav-arrows")
    var modalCloseButton = document.querySelector(".modal-close")
    var body = document.querySelector("body")
    var filter = document.querySelector(".filter")

    var tl = new TimelineMax({ paused: true })
    tl
      .set(body, { overflowY: "inherit", position: "inherit" })
      .to(postModal, 0.5, { autoAlpha: 0 })
      .to(postNav, 0.5, { autoAlpha: 0 }, "-=0.5")
      .to(modalContainer, 0.5, { display: "none", autoAlpha: 0 })

    document.onkeydown = function (evt) {
      evt = evt || window.event
      var isEscape = false
      if ("key" in evt) {
        isEscape = evt.key == "Escape" || evt.key == "Esc"
      } else {
        isEscape = evt.keyCode == 27
      }
      if (isEscape) {
        tl.play(0)
        const canonical = document.querySelector("link[rel=canonical]")
        document.head.removeChild(canonical)
        history.pushState("", document.title, window.location.pathname + window.location.search)
      }
    }

    var closeMethodsArray = [filter, modalCloseButton]

    closeMethodsArray.forEach(function (close) {
      close.addEventListener("click", function () {
        tl.play(0)
        const canonical = document.querySelector("link[rel=canonical]")
        document.head.removeChild(canonical)
        history.pushState("", document.title, window.location.pathname + window.location.search)

      })
    })
  }
  removeBackgroundWash()


  /**
   *
   * Adds sayings that randomly propogate on the Instagram post thumbnails.
   *
   *
   */

  var randomInstaSubs = function () {
    var instaQuotes = [
      "Open Me!",
      "Click if you dare.",
      "Check it out.",
      "Insta-Happiness"
    ]

    var instaSubs = document.querySelectorAll(".instagram-subtitle")

    for (var i = 0; i < instaSubs.length; i++) {
      var randomNum = Math.floor(Math.random() * instaQuotes.length)
      var randomInstaQuote = instaQuotes[randomNum]

      instaSubs[i].innerHTML += randomInstaQuote
    }
  }
  randomInstaSubs()



  const loadMoreRotate = function () {

    const loadMoreButton = document.querySelector(".loadmore")

    const spin = new TimelineMax({ paused: true })

    spin.to(loadMoreButton, 1, {
      transformOrigin: "50% 50%",
      rotation: -1485,
      ease: Elastic.easeInOut.config(1, 0.3)
    })

    loadMoreButton.addEventListener("mouseenter", function () {
      spin.play(loadMoreButton)
    })
  }

  loadMoreRotate()



} //End Screenwidth conditional
