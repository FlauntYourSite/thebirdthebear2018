/**
 * Does the question flip on the QA page.
 *
 */

const questionFlip = function() {
  const questions = document.querySelectorAll(".question")

  questions.forEach(function(question) {
    const link = question.parentElement
    const answer = question.parentElement.nextElementSibling
    const answerHeight = answer.offsetHeight
    const x = question.previousElementSibling

    answer.style.display = "none"
    answer.style.opacity = 0
    answer.style.height = 0
    answer.style.transform = "translateX(100vw)"

    const tl = new TimelineMax()
    tl
      .to(answer, 0.5, {
        display: "block",
        height: answerHeight,
        ease: Elastic.easeOut.config(1, 0.3)
      })
      .to(
        answer,
        0.75,
        {
          opacity: 1,
          transform: "translateX(0)",
          ease: Elastic.easeOut.config(1, 1)
        },
        "-=.5"
      )
      .to(x, 0.5, { rotation: -5, ease: Elastic.easeOut.config(1, 1) }, "-=.75")
      .reverse()

    link.addEventListener("click", toggleAnimation)

    function toggleAnimation() {
      if (tl.reversed()) {
        tl.play()
      } else {
        tl.reverse()
      }
    }
  })
}
questionFlip()

