/**
 * Fade in Bear and Vine in first section.
 *
 */
var vineDraw = function () {
  var homeOne = document.querySelector("#home-one")
  var birdBear = document.querySelector("#bird-bear")
  var maskLeft = document.querySelector("#left-line")
  var maskRight = document.querySelector("#right-line")

  var tl = new TimelineMax()

  tl
    .from([maskLeft, maskRight], 1.5, { drawSVG: 1, ease: Power4.easeInOut })
    .fromTo(birdBear, 1.5, { autoAlpha: 0 }, { autoAlpha: 1 }, "-=1")

  var scene = new ScrollMagic.Scene({ triggerElement: homeOne, offset: -120 })
    .addTo(controller)
    .setTween(tl)
}

vineDraw()

/**
 *
 * Make It Rain horizontally for devices larger than mobile.
 *
 */

function makeItRain() {

  const helloFriends = document.querySelector(".hello-friends")
  const section = document.querySelector("#horizontal-rain")
  const rain = document.querySelectorAll(".home-rain")

  const tl = new TimelineMax();
  tl.from(helloFriends, 1.5, { transformOrigin: "50% 50%", scale: 0, ease: Power4.easeOut })
    .to(helloFriends, .75, { rotationY: 90 }, 4)

  // Create the Scrollmagic effect
  var scene = new ScrollMagic.Scene({
    triggerElement: helloFriends,
    offset: -200
  })
    .addTo(controller)
    .setTween(tl)


  for (var i = 0; i < rain.length; i++) {
    rain[i].style.setProperty("top", Math.floor(Math.random() * 40) + "vh")

    const tl2 = new TimelineMax({ repeat: -1 })
      .fromTo(rain[i], Math.floor(Math.random() * (20 - 15 + 1) + 15),
        { left: "-" + 60 + "vw" },
        { transform: "translateX(180vw)", ease: Linear.easeNone }, Math.random() * 15
      )
  }

}
makeItRain()



/**
 *
 * Draws Frilly circles
 *
 */

const pinkCircleDraw = function () {
  var circles = Array.prototype.slice.call(document.querySelectorAll(".pink-circle-svg"));

  circles.forEach(circleDraw);

  function circleDraw(group, index) {

    var masks = group.querySelectorAll(".circle-mask");
    var paths = group.querySelectorAll(".mask-path");
    var layers = group.querySelectorAll(".layer-circle-sketch");

    masks.forEach(function (mask, i) {
      var id = "circle-" + index + "-mask-" + i;
      mask.id = id;
      layers[i].setAttribute("mask", "url(#" + id + ")");
    });

    var tl = new TimelineMax()
    tl.set(paths[0], { transformOrigin: "50% 50%", rotation: 180 })
      .from(paths[0], 1.5, { drawSVG: 1, ease: Power4.easeInOut })

    // Create the Scrollmagic effect
    var scene = new ScrollMagic.Scene({
      triggerElement: group,
      offset: -120
    })
      .addTo(controller)
      .setTween(tl)
  }
}
pinkCircleDraw();





/**
 *
 * Left and Right Arrow Bounce
 *
 */


const arrowBounce = function () {
  const leftArrow = document.querySelectorAll(".right-arrow-bounce")
  if (leftArrow) {
    const rightArrow = document.querySelectorAll(".left-arrow-bounce")
    const circleSign = document.querySelectorAll(".button-white")

    for (var i = 0; i < leftArrow.length; i++) {
      // 2. Timeline
      const tlTrigger = new TimelineMax({ repeat: 1 })
      tlTrigger
        .to(rightArrow[i], 0.25, { x: 40, ease: Power2.easeIn })
        .to(rightArrow[i], 0.25, { x: 0, ease: Elastic.easeOut.config(2, 1) }, "-=.1")
        .to(leftArrow[i], 0.25, { x: -40, ease: Power2.easeIn }, "-=.40")
        .to(leftArrow[i], 0.25, { x: 0, ease: Elastic.easeOut.config(2, 1) }, "-=.2")

      // 2. Curtain Scene
      const scene = new ScrollMagic.Scene({
        triggerElement: leftArrow[i],
        offset: -100,
        reverse: false
      })
        .addTo(controller)
        .setTween(tlTrigger)
    }

  }
}
arrowBounce()


/**
 *
 * Loops over all buttons and animates the draw effect.
 *
 */

const buttonDraw = function () {
  var whiteButtons = Array.prototype.slice.call(document.querySelectorAll(".button-frame"));
  if (whiteButtons) {
    whiteButtons.forEach(buttonGroup);

    function buttonGroup(group, index) {

      var masks = group.querySelectorAll(".mask");
      var paths = group.querySelectorAll(".mask-path");
      var layers = group.querySelectorAll(".layer")

      masks.forEach(function (mask, i) {
        var id = "button-" + index + "-mask-" + i;
        mask.id = id;
        layers[i].setAttribute("mask", "url(#" + id + ")");
      });

      const tl = new TimelineMax()
      tl.from(paths, 1, { drawSVG: 1, ease: Power2.easeInOut })
        .add(arrowBounce, "-=1")


      // 2. Curtain Scene
      var scene = new ScrollMagic.Scene({ triggerElement: group, offset: -100 })
        .addTo(controller)
        .setTween(tl)

      group.addEventListener("mouseover", function () {
        if (!tl.isActive()) {
          tl.play(0)
        }
      })

    }
  }
}
buttonDraw();



/**
 *
 * staggers the instagram thumbnails in the footer.
 *
 */

const instaFooterFade = function () {
  const instaThumbsRow = document.querySelector(".insta-row")
  const instaThumbs = document.querySelectorAll(".insta-row img")

  instaThumbs.forEach(function (instaThumb) {
    instaThumb.style.visibility = "hidden"
  })
  const tl = new TimelineMax()

  tl.staggerFrom(instaThumbs, 0.5, { scale: 0.95, y: 110, autoAlpha: 0 }, 0.1)

  // 2. Curtain Scene
  const scene = new ScrollMagic.Scene({
    triggerElement: instaThumbsRow,
    offset: -220
  })
    .addTo(controller)
    .setTween(tl)
  //   })
}
instaFooterFade()

const texasAnimate = function () {

  const outline = document.querySelector('#texas-outline-mask');
  const branchOne = document.querySelector('#texas-branch-one-mask');
  const branchTwo = document.querySelector('#texas-branch-two-mask');
  const heart = document.querySelector('#texas-heart-mask');

  const tl = new TimelineMax();

  tl.from(outline, 1, { drawSVG: 1, ease: Power4.easeIn, })
    .from(branchOne, .5, { drawSVG: 1, ease: Power2.easeInOut })
    .from(branchTwo, .5, { drawSVG: 1, ease: Power2.easeInOut })
    .from(heart, .5, { drawSVG: 1, ease: Power2.easeInOut }, "-=.25")

  const scene = new ScrollMagic.Scene({
    triggerElement: outline,
    offset: -350
  })
    .addTo(controller)
    .setTween(tl)

}
texasAnimate()



/**
 * Set Branches
 * 
 */

const branchbla = function () {
  const fourBranch = document.querySelectorAll('.row .branch-group');

  for (var i = 0; i < fourBranch.length; i++) {
    if ((i % 2) === 0) {
      fourBranch[i].style.cssText = "position: absolute;transform: rotate(50deg);width: 250px;top: -50px;left: -50px;z-index: -1;";
    }

    if ((i % 2) !== 0) {
      fourBranch[i].style.cssText = "position: absolute;transform: rotate(-50deg);width: 250px;bottom: -160px;right: 0;z-index: -1;";
    }
  }
};
branchbla()



/**
 *
 * Curved Arrow Animation
 *
 */

const curvedArrowAnimate = function () {

  const curvedArrow = document.querySelectorAll("#curved-arrow")
  const texas = document.querySelectorAll(".texas")

  if (curvedArrow) {

    const tl = new TimelineMax()
    tl.set(curvedArrow, { transformOrigin: "100% 0" })
      .from(curvedArrow, 1.5, { autoAlpha: 0, rotation: 180, ease: Elastic.easeOut.config(.5, .5) })

    // 2. Curtain Scene
    const scene = new ScrollMagic.Scene({ triggerElement: texas, offset: -300 })
      .addTo(controller)
      .reverse(false)
      .setTween(tl)
  }

}

curvedArrowAnimate()


