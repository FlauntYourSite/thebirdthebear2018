jQuery(function ($) {
    $('.loadmore').on('click', function () {

        var button = $(this),
            data = {
                'action': 'loadmore',
                'query': loadmore_params.posts, // that's how we get params from wp_localize_script() function
                'page': loadmore_params.current_page
            };

        $.ajax({
            url: loadmore_params.ajaxurl, // AJAX handler
            data: data,
            type: 'POST',
            success: function (data) {
                if (data) {
                    button.before(data); // insert new posts
                    loadmore_params.current_page++;

                    if (loadmore_params.current_page == loadmore_params.max_page)
                        button.remove(); // if last page, remove the button
                } else {
                    button.remove(); // if no data, remove the button as well
                }
            }
        });

    });
});