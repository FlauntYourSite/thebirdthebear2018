<?php
  $slides = get_field( 'section_2_gallery' );
					
  if( $slides ): ?>

        <?php foreach( $slides as $slide ): {?>

            <div class="home-rain">
                <?php echo wp_get_attachment_image( $slide['ID'], 'medium_large', false, array( 'alt' => $slide['alt'] ) ); ?>
            </div>
        <?php } ?>

    <?php endforeach; ?>

<?php endif; ?>
