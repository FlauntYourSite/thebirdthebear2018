<?php
/**
 * TheBirdTheBear2018 functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package TheBirdTheBear2018
 */

if ( ! function_exists( 'thebirdthebear2018_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function thebirdthebear2018_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on TheBirdTheBear2018, use a find and replace
		 * to change 'thebirdthebear2018' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'thebirdthebear2018', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'thebirdthebear2018' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'thebirdthebear2018_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'thebirdthebear2018_setup' );


/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function thebirdthebear2018_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'thebirdthebear2018' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'thebirdthebear2018' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'thebirdthebear2018_widgets_init' );



function add_thumbnail_to_JSON() {
	//Add featured image
	register_rest_field( 
		'post', // Where to add the field (Here, blog posts. Could be an array)
		'featured_image_src', // Name of new field (You can call this anything)
		array(
			'get_callback'    => 'get_image_src',
			'update_callback' => null,
			'schema'          => null,
			 )
		);
	}
	
	function get_image_src( $object, $field_name, $request ) {
	  $feat_img_array = wp_get_attachment_image_src(
		$object['featured_media'], // Image attachment ID
		'thumbnail',  // Size.  Ex. "thumbnail", "large", "full", etc..
		true // Whether the image should be treated as an icon.
	  );
	  return $feat_img_array[0];
	}
   
	add_action( 'rest_api_init', 'add_thumbnail_to_JSON' );



/**
 * Enqueue scripts and styles.
 */
function thebirdthebear2018_scripts() {
	wp_enqueue_script( 'jquery' ); 
	wp_enqueue_style( 'thebirdthebear2018-style', get_stylesheet_uri(), array(), '20180522');

	wp_enqueue_style( 'google_fonts', '//fonts.googleapis.com/css?family=PT+Serif|Questrial' );

	wp_enqueue_script( 'thebirdthebear2018-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20180522', true );

	//Adds Greensock support
	wp_enqueue_script( 'greensock_drawSVG', get_template_directory_uri() . '/js/DrawSVGPlugin.min.js', array('jquery'), '20180522', true );
	wp_enqueue_script( 'greensock', '//cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/TweenMax.min.js', array('jquery'), '20180522', true);

	//Adds Scrollmagik support
	wp_enqueue_script( 'scrollmagic', '//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js', array('jquery'), '20180522' );
	// wp_enqueue_script( 'scrollmagic_indicators', '//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min.js', array(), '20180522' );
	wp_enqueue_script( 'scrollmagic_gsap_support', '//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.min.js', array('jquery'), '20180522' );

	//Adds Swiper Slider Styles
	wp_enqueue_style( 'swiper_styles', '//cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.0/css/swiper.min.css' );
	//Adds Swiper Slider Scripts
	wp_enqueue_script( 'swiper_scripts', '//cdnjs.cloudflare.com/ajax/libs/Swiper/4.1.0/js/swiper.min.js', array(), '20180522', false );

	//Adds Plyr HTML5 video container support
	wp_enqueue_style( 'plyr_scripts', '//cdn.plyr.io/3.2.3/plyr.css' );
	wp_enqueue_script( 'plyr_scripts', '//cdn.plyr.io/3.2.3/plyr.js', array(), '20180522', false );


	//Calls the Blog page scripts.
	if ( is_home() ){
		wp_enqueue_script( 'thebirdthebear2018-blog-scripts', get_template_directory_uri() . '/js/blog-page.js', array('jquery' ), '20180522', true );
	}

	//Calls the Service page scripts.
	if ( 'services' == get_post_type() ){
		wp_enqueue_script( 'thebirdthebear2018-services-scripts', get_template_directory_uri() . '/js/services-min.js', array(), '20180522', true );
	}

	//Calls the FAQ page scripts.
	if ( is_page( 'our-process' ) ){
		wp_enqueue_script( 'thebirdthebear2018-faq-scripts', get_template_directory_uri() . '/js/qa-min.js', array(), '20180522', true );
	}
	


	//Scripts for Home page
	if ( is_front_page() ){
		wp_enqueue_script( 'thebirdthebear2018-custom-eases', get_template_directory_uri() . '/js/CustomEase.min.js', array(), '20180522', true );
		wp_enqueue_script( 'thebirdthebear2018-home', get_template_directory_uri() . '/js/home-min.js', array(), '20180522', true );
	}

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	//Calls the Header and Menu script.
	wp_enqueue_script( 'thebirdthebear2018-header', get_template_directory_uri() . '/js/header-min.js', array(), '20180522', true );

 
}
add_action( 'wp_enqueue_scripts', 'thebirdthebear2018_scripts' );
add_action( 'login_enqueue_scripts', 'thebirdthebear2018_scripts' );




function tbbt_custom_login() {
	
			wp_enqueue_style( 'tbbt-custom-login-css', get_template_directory_uri() . '/login/tbbt-login-min.css' );
			wp_enqueue_script( 'tbbt-custom-login', get_template_directory_uri() . '/login/tbbt-login-min.js', array(), '20180522', true );
			wp_enqueue_script( 'wp-api' );
			wp_localize_script( 'tbbt-custom-login', 'tbbt_custom_login_ajaxify', array( 'currentSite' => get_bloginfo( 'url' ) ) );
		}
	
add_action( 'login_enqueue_scripts', 'tbbt_custom_login' );
	
	


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Loads Custom Post Types.
 */
require get_template_directory() . '/inc/custom-post-types.php';



/**
 * Add additional images. 
 * 
 */

add_image_size( 'Oversized', '1400', '1400', false ); 
add_image_size( 'Fullscreen', '2000', '2000', false ); 

add_filter('max_srcset_image_width', function($max_srcset_image_width, $size_array){
    return 2000;
}, 10, 2);




/**
 * REMOVES WP EMOJI
 */
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );


/**
 * REMOVES WP-Embed
 */
function my_deregister_scripts(){
	wp_deregister_script( 'wp-embed' );
  }
  add_action( 'wp_footer', 'my_deregister_scripts' );


/**
 * 
 * Divide post titles by the PIPE
 * 'Couple' echoes Couple Name
 * 'location' echoes Location (or anthing after Couple)
 */

function couple(){
	$title = get_the_title();
	echo substr($title, strpos($title, " | ") + 3);
 }

 function location(){
	$title = get_the_title();
	echo substr($title, 0, strrpos($title, " | "));
 }



/**
 * Controls the output of Social Media icons from the 
 * 
 */

function tbtb_social( $social_network, $social_url, $social_title ){

	if( isset( $social_network ) ): { ?>

		<a class="social-icon" target="_blank" rel="noopener" href="<?php echo esc_url( $social_url ) ?>" title="<?php echo $social_title ?>">
			<svg class="fs-icons">
				<use xlink:href="#icon-<?php echo $social_network ?>"></use>
			</svg>
		</a>

	<?php } 
	endif;
}

function fsc_figure( $image, $size, $imageclass, $captionclass ){

	/**
	* Let plugins pre-filter the image meta to be able to fix inconsistencies in the stored data.
	*
	* @param string 		$image    			The ACF field name (i.e. 'your_photo_name').
	* @param string  		$size    				Thumbnail size (i.e. 'Thumbnail', 'Medium', 'Large')
	* @param string 		$imageclass     The Figure class you want to use (ex: 'my-figure') 
	* @param string    	$captionclass 	The Figcaption class you want to use (ex: 'caption-blue') 
	*/

		$image = get_field( $image );
		$size = $size;
		$thumb = $image['sizes'][ $size ];

		if( !empty($image) ):  ?>

			<figure class="<?php echo $imageclass; ?>">
			
				<?php echo wp_get_attachment_image( $image['ID'], $size, false, array( 'alt' => $image['alt'] ) );  ?>

					<figcaption class="<?php echo $captionclass; ?>">

						<?php echo $image[ 'caption' ]; ?>

					</figcaption>

			</figure>

			<script type="application/ld+json">
				{
				"@context": "http://schema.org",
				"@type": "ImageObject",
				"author": "The Bird & The Bear",
				"contentUrl": "<?php echo $thumb; ?>",
				"datePublished": "<?php echo $image[ 'created_timestamp' ]; ?>",
				"description": "<?php echo $image[ 'alt' ]; ?>",
				"name": "<?php echo $image[ 'title' ]; ?>",
				"copyright": "©The Bird & The Bear"
				}
			</script>

		<?php endif; 
}


function tbtb_schema_main(){	?>

	<script type='application/ld+json'>
		{
			"@context": "http://www.schema.org",
			"@type": "ProfessionalService",
			"name": "The Bird & The Bear | Photography & Films",
			"url": "<?php echo esc_url( home_url( '/' ) ); ?>",
			"image": "<?php echo get_template_directory_uri(); ?>/screenshot.png",
			"logo": "<?php echo get_template_directory_uri(); ?>/images/the-bird-the-bear-logo.png",
			"telephone": "(512) 710-5123",

			"address": {
				"@type": "PostalAddress",
				"streetAddress": "823 Congress Ave ste 150-1081",
    			"addressLocality": "Austin",
    			"addressRegion": "TX",
    			"postalCode": "78767",
    			"addressCountry": "United States"
			},

			"sameAs": [
				"https://facebook.com/thebirdthebear'",
				"https://twitter.com/thebirdthebear",
				"https://www.instagram.com/thebirdthebear/",
				"https://pinterest.com/carynloves/"
			]
		}
	</script>
<?php }

add_action( 'wp_enqueue_scripts', 'tbtb_schema_main' );



  function white_button( $text ){ ?>

    <svg class="button-frame" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 187.65 70.58" preserveAspectRatio="none">
		<defs>
			<mask id="button-mask-one" class="mask"> 
				<path class="layeronemask mask-path" fill="none" stroke="#fff" stroke-miterlimit="20" stroke-width="15" d="M8.49.28l-3.5 62.5"/>
			</mask>
			<mask id="button-mask-two" class="mask"> 
				<path class="layertwomask mask-path" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="16" d="M.49 57.28l187 5"/>
			</mask>
			<mask id="button-mask-three" class="mask"> 
				<path class="layerthreemask mask-path" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="15" d="M173.49 70.28l4-66" />
			</mask>
			<mask id="button-mask-four" class="mask"> 
				<path class="layerfourmask mask-path" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="14" d="M184 6.78H.99"/>
			</mask>
		</defs>

      
      <?php if ($text == Inquire ){ ?>
        <g id="text" fill="#fff">
          <path d="M36.91 24.66h1.2v16.16h-1.2z"/>
          <path fill-rule="evenodd" d="M57.04 24.66v14.81L46.31 24.66H44.8v16.01H46V26.01l10.73 14.81h1.51V24.81l-1.2-.15z"/>
          <path fill-rule="evenodd" d="M76.66 43.69A6.52 6.52 0 0 1 69.77 37a6.89 6.89 0 0 1 13.77 0 6.51 6.51 0 0 1-6.88 6.69zm8.24 2.74a2.65 2.65 0 0 1-2.4 1.49 4.6 4.6 0 0 1-4.21-3c4.32-.49 6.44-3.67 6.44-7.93C84.73 32.2 82 29 76.66 29s-8.08 3.2-8.08 8 2.61 7.89 7.71 8.09a6.23 6.23 0 0 0 6.2 4.07 5.84 5.84 0 0 0 3.39-1z" transform="translate(-5.01 -4.22)"/>
          <path fill-rule="evenodd" d="M102.31 28.88v9.41c0 3.81-2.74 5.4-5.54 5.4s-5.51-1.59-5.51-5.4v-9.41h-1.19v9.36c0 4.36 2.28 6.65 6.7 6.65s6.73-2.31 6.73-6.65v-9.36z" transform="translate(-5.01 -4.22)"/>
          <path d="M106.55 24.66h1.2v16.16h-1.2z"/>
          <path fill-rule="evenodd" d="M120.64 30.23h5.78c2.34 0 3.9 1.33 3.9 3.48 0 2.64-2.23 3.33-4 3.33h-5.68zm0 8h5.71l4 6.8h1.54l-3.64-7c1.83-.28 3.3-1 3.3-4.31S130 29 126.65 29h-7.21v16h1.2z" transform="translate(-5.01 -4.22)"/>
          <path fill-rule="evenodd" d="M144.49 26.01v-1.2H133.2v15.86h11.29v-1.2H134.4v-5.45h9v-1.2h-9v-6.81h10.09z"/>
        </g>
      <?php } elseif ($text == Learn_More ){ ?>
        <g id="Learn_More" fill="#fff" fill-rule="evenodd" data-name="Learn More">
          <path d="M11.92 23.24v14.64h9.18v-1.1h-8.08V23.24h-1.1z"/>
          <path d="M36.92 24.47v-1.09H26.6v14.5h10.32v-1.1h-9.23V31.8h8.23v-1.1h-8.23v-6.23h9.23z"/>
          <path d="M50.39 38l3-7.48 3 7.48zm4.08-8.71h-2.11L46.07 44h1.86l1.82-5h7.34l1.81 5h1.87z" transform="translate(-6 -6)"/>
          <path d="M66.78 30.47h5.28c2.14 0 3.57 1.22 3.57 3.18 0 2.42-2 3.05-3.66 3.05h-5.19zm0 7.33H72l3.63 6.2H77l-3.3-6.4c1.68-.26 3-1 3-4s-1.37-4.27-4.46-4.27h-6.56V44h1.1z" transform="translate(-6 -6)"/>
          <path d="M88.04 23.24v13.54l-9.82-13.54h-1.38v14.64h1.1V24.47l9.81 13.55h1.38V23.38l-1.09-.14z"/>
          <path d="M114.63 23.24l-5.35 7-5.45-7h-1.36v14.78h1.09V25.6l5.72 7.14 5.62-7.14v12.42h1.1V23.28l-1.37-.04z"/>
          <path d="M135.51 42.78a6 6 0 0 1-6.3-6.15 6.3 6.3 0 0 1 12.59 0 6 6 0 0 1-6.29 6.15zm0-13.4c-4.86 0-7.39 2.89-7.39 7.25s2.53 7.25 7.39 7.25 7.38-2.9 7.38-7.25-2.53-7.25-7.38-7.25z" transform="translate(-6 -6)"/>
          <path d="M150.11 30.47h5.28c2.14 0 3.57 1.22 3.57 3.18 0 2.42-2 3.05-3.65 3.05h-5.2zm0 7.33h5.22L159 44h1.41L157 37.6c1.67-.26 3-1 3-4s-1.37-4.27-4.46-4.27H149V44h1.1z" transform="translate(-6 -6)"/>
          <path d="M170.5 24.47v-1.09h-10.32v14.5h10.32v-1.1h-9.23V31.8h8.23v-1.1h-8.23v-6.23h9.23z"/>
        </g>
      <?php } ?>

      <path mask="url(#button-mask-one)" class="layer" id="button-left" d="M12 7.8v.39c-.12 2.84-.25 5.77-.62 8.66l-.23 1.62c-.12.83-.23 1.65-.33 2.48 0 .49-.07 1-.1 1.47a18.24 18.24 0 0 1-.22 2.3 13.26 13.26 0 0 0-.15 1.57c0 .46 0 .92-.11 1.37-.15 1-.24 2.11-.29 3.19-.16 3.6-.29 7.21-.42 10.83v3.49a50.96 50.96 0 0 1-.26 3.87 11.24 11.24 0 0 0 0 2A10.92 10.92 0 0 1 9 53.89a8.46 8.46 0 0 1 .07 2.92 9.3 9.3 0 0 0 0 2.17.43.43 0 0 1 0 .16C8.84 62.29 10.45 67 10.45 67a91.8 91.8 0 0 1 0-7.42l.07-1.43a25.39 25.39 0 0 0-.05-4.86 6.15 6.15 0 0 1 0-1v-3.57c0-2.92.06-5.84.12-8.76v-.67c0-2 .08-4 .31-6s.35-3.84.47-5.72l.22-3.12c.13-1.61.33-3.24.53-4.82a75.96 75.96 0 0 0 .67-7.93l.08-1.77L13 7c.12-2.6-1 .8-1 .8z" class="cls-1" transform="translate(-5.01 -4.22)"/>
      <path mask="url(#button-mask-two)" class="layer" id="button-bottom" d="M7.61 62H6a14.57 14.57 0 0 0 1.91 1.68c1.15.74 2.78 1.14 5.47 1.3.75 0 1.53.06 2.4.08l17.92.32h.86c6.3.1 12.81.21 19.23.54l3.6.2c1.84.1 3.67.2 5.51.28 1.07 0 2.16.07 3.24.09 1.68 0 3.41.07 5.11.19 1.14.08 2.29.11 3.5.14 1 0 2 0 3 .1 2.31.12 4.69.2 7.09.25l24 .36h7.75c2.82 0 5.73.1 8.6.23 1.44.07 2.9 0 4.45 0A61.05 61.05 0 0 1 136 68a50.27 50.27 0 0 1 6.48-.06c1.69.06 3.28.12 4.82 0h.35c7 .2 14.16 0 21.1-.12l2.07-.05a79.21 79.21 0 0 0 15.9-1.67c1.88-.43 2.69-.87 2.82-1.55.12-.84.42-2.47.42-2.47l-1.82.64-.08.48a4.61 4.61 0 0 1-.14.62c-.17.54-1 1-2.47 1.36a51.15 51.15 0 0 1-7.79 1.18c-1.95.16-4.53.35-7.13.37h-5.43c-5.4.06-11 .12-16.48 0l-3.18-.06c-3.73-.08-7.24-.15-10.78 0h-10.23c-6.5 0-13 0-19.45-.1h-1.5c-4.36 0-8.86-.07-13.26-.27-4.22-.19-8.53-.3-12.7-.41l-6.92-.19c-3.58-.12-7.2-.29-10.69-.46a380.23 380.23 0 0 0-17.61-.59l-3.94-.07-6.6-.13c-5.67-.11-11.32-.23-17-.31-2.86 0-4.83-.47-6.05-1.31L7.61 62z" class="cls-1" transform="translate(-5.01 -4.22)"/>
      <path mask="url(#button-mask-three)" class="layer" id="button-right" d="M180 70.2v-.39c.12-2.84.25-5.77.62-8.66l.23-1.62c.12-.83.23-1.65.33-2.48 0-.49.07-1 .1-1.47a18.24 18.24 0 0 1 .22-2.3 13.26 13.26 0 0 0 .15-1.57c0-.46 0-.92.11-1.37.15-1 .24-2.11.29-3.19.16-3.6.29-7.21.42-10.83v-2.59l.05-.9c0-1.27.11-2.58.26-3.87a11.24 11.24 0 0 0 0-2 10.92 10.92 0 0 1 .12-2.84 8.46 8.46 0 0 1-.07-2.92 9.3 9.3 0 0 0 0-2.17.43.43 0 0 1 0-.16c.22-3.15-1.39-7.86-1.39-7.86a91.8 91.8 0 0 1 0 7.42l-.07 1.43a25.39 25.39 0 0 0 .05 4.86 6.15 6.15 0 0 1 0 1v3.57c0 2.92-.06 5.84-.12 8.76v.67c0 2-.08 4-.31 6s-.35 3.84-.47 5.72l-.22 3.12c-.13 1.61-.33 3.24-.53 4.82a75.96 75.96 0 0 0-.67 7.93l-.08 1.77-.15 3c.01 2.52 1.13-.88 1.13-.88z" class="cls-1" transform="translate(-5.01 -4.22)"/>
      <path mask="url(#button-mask-four)" class="layer" id="button-top" d="M177.76 11h-1.12c-8.22-.12-16.74-.25-25.12-.62l-4.72-.23a826.4 826.4 0 0 0-7.19-.33c-1.4 0-2.82-.07-4.24-.1-2.19 0-4.46-.08-6.68-.22-1.49-.09-3-.12-4.57-.15-1.32 0-2.65 0-4-.11-3-.15-6.13-.24-9.27-.29q-15.64-.24-31.41-.42H69.35a428.4 428.4 0 0 1-11.24-.26c-1.88-.08-3.79 0-5.82 0-2.67 0-5.43.09-8.25-.12a73 73 0 0 1-8.48.07c-2.2-.06-4.28-.13-6.29 0h-.46C19.66 7.84 6 9.45 6 9.45c7.06-.07 14.33-.13 21.53 0l4.16.07c4.87.09 9.46.17 14.09-.05 1-.05 2-.05 3 0h10.34c8.49 0 17 .06 25.41.12h2c5.69 0 11.58.08 17.33.31a936 936 0 0 0 16.58.47l9.05.22c4.69.13 9.41.33 14 .53a638.24 638.24 0 0 0 23.01.67l5.15.08 8.62.15c7.39.1-2.51-1.02-2.51-1.02z" class="cls-1" transform="translate(-5.01 -4.22)"/>

    </svg>


<?php } 





function load_more_posts_script() {

	global $wp_query;

	wp_register_script( 'my_loadmore', get_template_directory_uri() . '/js/loadmore.js', array('jquery'), true );

	wp_localize_script( 'my_loadmore', 'loadmore_params', array(
		'ajaxurl' 			=> site_url() . '/wp-admin/admin-ajax.php', // WordPress AJAX
		'posts' 			=> json_encode( $wp_query->query_vars ), // everything about your loop is here
		'current_page' 		=> get_query_var( 'paged' ) ? get_query_var('paged') : 1,
		'max_page' 			=> $wp_query->max_num_pages,
	) );

	wp_enqueue_script( 'my_loadmore' );
}

add_action( 'wp_enqueue_scripts', 'load_more_posts_script' );

function loadmore_ajax_handler(){

	$sticky = get_option( 'sticky_posts' );
	$sticky_number = 40;

	// prepare our arguments for the query
	$args = json_decode( stripslashes( $_POST[ 'query' ] ), true );
	$args[ 'paged' ] 			= $_POST[ 'page' ] + 1; // we need next page to be loaded
	$args[ 'post_status' ] 		= 'publish';
	$args[ 'post_type' ] 		= array( 'post', 'instagram' );
	$args[ 'post__not_in' ] 	= get_option( 'sticky_posts' );
	$args[ 'posts_per_page' ] 	= $sticky_number;

	// it is always better to use WP_Query but not here
	query_posts( $args );
    
	if( have_posts() ) :

		// run the loop
		while( have_posts() ): the_post();
		?>
	
	<a href="<?php the_permalink(); ?>" class="thumb ajax-link" data-hash="<?php the_id(); ?>">
		<?php the_post_thumbnail( 'medium_large' ); ?>
		<?php get_template_part( 'inc/post-instagram-thumb' ); ?>

	</a>

        <?php
		endwhile;

	endif;
	die; // here we exit the script and even no wp_reset_query() required!
}

add_action('wp_ajax_loadmore', 'loadmore_ajax_handler'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_loadmore', 'loadmore_ajax_handler'); // wp_ajax_nopriv_{action}


/**
 * 
 * HTML5 Video Container
 * 
 */

function video_container( $image ){ 
	$image = get_field( $image );
	$poster = $image['url'];
	?> 
	
	<video class="plyr" poster="<?php echo $poster ?>">
		<source src="<?php echo get_post_meta( $image['ID'], 'video_embed_code', true );?>" type="video/mp4">
	</video>

<?php }


/**
 * 
 * Slideshow for Header, and Service pop-outs.
 * 
 */

function tbtb_slideshows( $slides, $class ){

	$slides = get_field( $slides );
	$iframe = get_field( 'video_embed_code');
	$class = $class;

	if( $slides ): ?>
		<!-- Slider main container -->
		<div class="swiper-container <?php echo $class; ?>">
			<!-- Additional required wrapper -->
			<div class="swiper-wrapper">
					<?php foreach( $slides as $slide ): ?>
						<?php if ( ! get_post_meta( $slide['ID'], 'video_embed_code', true )) { 
						
							echo wp_get_attachment_image( $slide['ID'], 'full', false, array( 'alt' => $slide['alt'], 'class' => 'swiper-slide', 'style' => 'object-position:' . (get_post_meta( $slide['ID'], 'mobile_image_center', true )) . '%;' ) );
						
						} elseif ( get_post_meta( $slide['ID'], 'video_embed_code', true ) ) { ?>
							<div class="swiper-slide">
								<video class="plyr" poster="<?php echo wp_get_attachment_url( $slide['ID'] ); ?>" controls>
									<source src="<?php echo get_post_meta( $slide['ID'], 'video_embed_code', true ); ?>" type="video/mp4">
								</video>
							</div>
						<?php }

					endforeach; ?>
			</div>

			<?php if( count($slides) > 1 ){ ?>
			<div class="swiper-pagination"></div> 
			<!-- If we need navigation buttons -->
			<div class="swiper-button-prev"></div>
			<div class="swiper-button-next"></div>
			<?php } ?>
			<a class="arrow-down"><?php require( 'images/arrow-down.svg' ); ?></a>
		</div>

	<?php endif; 

}

 
function prev_next_combine_posts_and_page( $where ){
	// $where looks like WHERE p.post_date < '2017-08-02 09:07:03' AND p.post_type = 'post' AND ( p.post_status = 'publish' OR p.post_status = 'private' )
	// In code $where looks like $wpdb->prepare( "WHERE p.post_date $op %s AND p.post_type = %s $where", $post->post_date, $post->post_type )
	// Parameters $op and another $where can not be passed to this action hook
	// So, I think the best way is to use str_replace()
	return str_replace(
		array( "p.post_type = 'post'", "p.post_type = 'instagram'" ),
		"(p.post_type = 'post' OR p.post_type = 'instagram')",
		$where
	); 
}

add_action( 'get_previous_post_where',  'prev_next_combine_posts_and_page', 20 );
add_action( 'get_next_post_where',  'prev_next_combine_posts_and_page', 20 );