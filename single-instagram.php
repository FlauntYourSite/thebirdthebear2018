<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package TheBirdTheBear2018
 */

get_header(); ?>
	<div class="para-hero"><img src="<?php the_field( 'header_image', get_option('page_for_posts') ); ?>"></div>  
	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) : the_post(); ?>


			<article id="instagram-<?php the_ID(); ?>" <?php post_class(); ?>>

				<header class="entry-header">
						<?php get_template_part( 'inc/animated-title' ); ?>
				</header><!-- .entry-header -->
			
				<div class="entry-content">
					<?php the_post_thumbnail( 'large' ); ?>
					<div class="insta-content">
						<?php the_content(); ?>
					</div>
				</div><!-- .entry-content -->
			
				<footer class="entry-footer">
					<?php thebirdthebear2018_entry_footer(); ?>
				</footer><!-- .entry-footer -->

				<?php get_template_part( 'inc/insta-circle' ); ?>
				
			</article><!-- #post-<?php the_ID(); ?> -->
			

			<div class="post-nav-arrows">
				<?php the_post_navigation(
						array(
							'prev_text'	=> __( 'Older' ),
							'next_text'	=> __( 'Newer' ),
						)
				); ?>
			</div>

		<?php endwhile;?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
