<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package TheBirdTheBear2018
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
			<?php get_template_part( 'inc/animated-title' ); ?>
	</header><!-- .entry-header -->
	
	<div class="entry-content">
		<?php
			the_content( sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'thebirdthebear2018' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'thebirdthebear2018' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php thebirdthebear2018_entry_footer(); ?>
	</footer><!-- .entry-footer -->
	<script type="application/ld+json">
		{ "@context": "http://schema.org", 
		"@type": "BlogPosting",
		"headline": "<?php the_title(); ?>",
		"image": "<?php the_post_thumbnail_url( 'medium' ); ?>",
		"url": "<?php the_permalink(); ?>",
		"publisher": "The Bird & The Bear",
		"datePublished": "<?php echo get_the_date('c'); ?>",
		"description": "",
		"articleBody": "<?php echo wp_strip_all_tags( get_the_content() ); ?>",
			"author": {
				"@type": "Person",
				"name": "Steve"
			}
		}
	</script>

</article><!-- #post-<?php the_ID(); ?> -->
