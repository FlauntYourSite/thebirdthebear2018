<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package TheBirdTheBear2018
 */

get_header(); ?>
	
		<div class="para-hero"><img src="<?php the_field( 'header_image', get_option('page_for_posts') ); ?>"></div>  

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<header class="entry-header">
				<?php get_template_part( 'inc/animated-title' ); ?>
			</header><!-- .entry-header -->


		<?php 
		$sticky = get_option( 'sticky_posts' );
		$sticky_number = 40 - count( $sticky );
		$bndposts = new WP_Query( array(
			'post_type'			=> array( 'post', 'instagram' ), 
			'posts_per_page' 	=> $sticky_number, 
			'orderby' 			=> 'DSC'
			)  
		);


		if ( $bndposts->have_posts() ) : ?>
				<div class="post-container">

					<?php while ( $bndposts->have_posts() ) : $bndposts->the_post(); ?>

							<a href="<?php the_permalink(); ?>" class="thumb" class="ajax-link" data-hash="<?php the_id(); ?>">
								<?php the_post_thumbnail( 'medium_large' ); ?>
								<?php get_template_part( 'inc/post-instagram-thumb' ); ?>

							</a>	
				
					<?php endwhile; ?>

					<?php else : ?>

					<?php get_template_part( 'template-parts/content', 'none' ); ?>

				</div>

		<?php endif; ?>
		
		<a class="loadmore"></a>

		</main><!-- #main -->

	</div><!-- #primary -->


	<div class="modal-container">
		<div class="filter"></div>
		<div class="post-modal">
			<div class="top-bar">
				<a href="#" class="modal-close"></a>
			</div>
			<div class="modal-logo"><?php require( 'images/bird-bear-logo-small.svg'); ?></div>

			<div class="modal-content">
			<!-- Dynamically being pulled in by JS -->
			</div>
		</div>

		<div class="post-nav-arrows">
			<?php the_post_navigation(
						array(
							'prev_text'	=> __( '' ),
							'next_text'	=> __( '' ),
						)
				); ?>
		</div>
	</div>

<?php
get_footer();
