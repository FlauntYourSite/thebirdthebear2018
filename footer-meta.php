<?php
/**
 * The template for the Meta Footer
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package TheBirdTheBear2018
 */

?>


<footer class="footer-meta">

		<div class="site-info">

			<div><a href="/contact">hello@thebirdthebear.com</a> | Austin, TX</div>
			<?php get_template_part( 'social' ); ?>
		</div>


		<div class="badges">

			<span>As Seen On:</span>
			<?php $the_query = new WP_Query( 
				
				array(
						'post_type' 	=> 'featured-medals',
						'post_count' 	=> 3,

			) ); ?>

				<?php if ( $the_query->have_posts() ) : ?>

					<!-- pagination here -->

					<!-- the loop -->
					<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						<a href="<?php the_field( 'featured_link' ); ?>">
							<?php the_post_thumbnail( 'thumb' ); ?>
						</a>
					<?php endwhile; ?>
					<!-- end of the loop -->

					<!-- pagination here -->

					<?php wp_reset_postdata(); ?>

				<?php else : ?>
					<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>		

		</div>


</footer>	


