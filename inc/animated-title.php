<div class="animated-title">
    <?php require_once get_template_directory() . '/images/branch-left.svg'; ?>
        <h1>
            <?php 
                if (is_home() ){
                    echo 'Journal'; 
                }elseif ( is_singular( 'instagram' ) ) {
                    echo 'Instagram Entry';
                }elseif ( is_singular( 'services' ) ){
                    the_title(); 
                }elseif ( is_single() ){
                    couple();
                }elseif ( !is_404() ) {
                    the_title(); 
                }else{
                    echo 'Oops!';
                }
            ?>
        </h1>
    <?php require_once get_template_directory() . '/images/branch-right.svg'; ?>
</div>
<?php if (is_singular( 'post', 'instagram' ) ){ ?>
    <div class="single-sub-title">
        <h2 class="title"><?php location(); ?></h2>
    </div>
<?php } ?>