<?php 

/**
 * Register all Custom Post Types
 * 
 */


function tbtb_custom_post_type() {


// Register Services

$labels = array(
	'name'                  => _x( 'Services', 'Post Type General Name', 'thebirdthebear2018' ),
	'singular_name'         => _x( 'Service', 'Post Type Singular Name', 'thebirdthebear2018' ),
	'menu_name'             => __( 'Services', 'thebirdthebear2018' ),
	'name_admin_bar'        => __( 'Service', 'thebirdthebear2018' ),
	'archives'              => __( 'Services Archives', 'thebirdthebear2018' ),
	'attributes'            => __( 'Service Attributes', 'thebirdthebear2018' ),
	'parent_item_colon'     => __( 'Parent Service:', 'thebirdthebear2018' ),
	'all_items'             => __( 'All Services', 'thebirdthebear2018' ),
	'add_new_item'          => __( 'Add New Service', 'thebirdthebear2018' ),
	'add_new'               => __( 'Add New', 'thebirdthebear2018' ),
	'new_item'              => __( 'New Service', 'thebirdthebear2018' ),
	'edit_item'             => __( 'Edit Service', 'thebirdthebear2018' ),
	'update_item'           => __( 'Update Service', 'thebirdthebear2018' ),
	'view_item'             => __( 'View Service', 'thebirdthebear2018' ),
	'view_items'            => __( 'View Services', 'thebirdthebear2018' ),
	'search_items'          => __( 'Search Service', 'thebirdthebear2018' ),
	'not_found'             => __( 'Not found', 'thebirdthebear2018' ),
	'not_found_in_trash'    => __( 'Not found in Trash', 'thebirdthebear2018' ),
	'featured_image'        => __( 'Featured Image', 'thebirdthebear2018' ),
	'set_featured_image'    => __( 'Set featured image', 'thebirdthebear2018' ),
	'remove_featured_image' => __( 'Remove featured image', 'thebirdthebear2018' ),
	'use_featured_image'    => __( 'Use as featured image', 'thebirdthebear2018' ),
	'insert_into_item'      => __( 'Insert into Service', 'thebirdthebear2018' ),
	'uploaded_to_this_item' => __( 'Uploaded to this Service', 'thebirdthebear2018' ),
	'items_list'            => __( 'Services list', 'thebirdthebear2018' ),
	'items_list_navigation' => __( 'Services list navigation', 'thebirdthebear2018' ),
	'filter_items_list'     => __( 'Filter Services list', 'thebirdthebear2018' ),
);

$args = array(
	'label'                 => __( 'Services', 'thebirdthebear2018' ),
	'description'           => __( 'Add the types of photography you provide.', 'thebirdthebear2018' ),
	'labels'                => $labels,
	'supports'              => array( 'title', 'thumbnail', 'editor' ),
	'taxonomies'            => array( 'category' ),
	'hierarchical'          => false,
	'public'                => true,
	'show_ui'               => true,
	'show_in_menu'          => true,
	'menu_position'         => 5,
	'menu_icon'             => 'dashicons-camera',
	'show_in_admin_bar'     => true,
	'show_in_nav_menus'     => false,
	'can_export'            => true,
	'has_archive'           => false,
	'exclude_from_search'   => true,
	'publicly_queryable'    => true,
	'capability_type'       => 'page',
);

register_post_type( 'services', $args );




// Register Instagram Posts

$labels = array(
	'name'                  => _x( 'Instagram Post', 'Post Type General Name', 'thebirdthebear2018' ),
	'singular_name'         => _x( 'Instagram Post', 'Post Type Singular Name', 'thebirdthebear2018' ),
	'menu_name'             => __( 'Instagram Posts', 'thebirdthebear2018' ),
	'name_admin_bar'        => __( 'Instagram', 'thebirdthebear2018' ),
	'archives'              => __( 'Instagram Archives', 'thebirdthebear2018' ),
	'attributes'            => __( 'Instagram Attributes', 'thebirdthebear2018' ),
	'parent_item_colon'     => __( 'Parent Instagram:', 'thebirdthebear2018' ),
	'all_items'             => __( 'All Instagram', 'thebirdthebear2018' ),
	'add_new_item'          => __( 'Add New Instagram', 'thebirdthebear2018' ),
	'add_new'               => __( 'Add New', 'thebirdthebear2018' ),
	'new_item'              => __( 'New Instagram', 'thebirdthebear2018' ),
	'edit_item'             => __( 'Edit Instagram', 'thebirdthebear2018' ),
	'update_item'           => __( 'Update Instagram', 'thebirdthebear2018' ),
	'view_item'             => __( 'View Instagram', 'thebirdthebear2018' ),
	'view_items'            => __( 'View Instagram', 'thebirdthebear2018' ),
	'search_items'          => __( 'Search Instagram', 'thebirdthebear2018' ),
	'not_found'             => __( 'Not found', 'thebirdthebear2018' ),
	'not_found_in_trash'    => __( 'Not found in Trash', 'thebirdthebear2018' ),
	'featured_image'        => __( 'Featured Image', 'thebirdthebear2018' ),
	'set_featured_image'    => __( 'Set featured image', 'thebirdthebear2018' ),
	'remove_featured_image' => __( 'Remove featured image', 'thebirdthebear2018' ),
	'use_featured_image'    => __( 'Use as featured image', 'thebirdthebear2018' ),
	'insert_into_item'      => __( 'Insert into Instagram', 'thebirdthebear2018' ),
	'uploaded_to_this_item' => __( 'Uploaded to this Instagram', 'thebirdthebear2018' ),
	'items_list'            => __( 'Instagram list', 'thebirdthebear2018' ),
	'items_list_navigation' => __( 'Instagram list navigation', 'thebirdthebear2018' ),
	'filter_items_list'     => __( 'Filter Instagram list', 'thebirdthebear2018' ),
);


$args = array(
	'label'                 => __( 'Instagram', 'thebirdthebear2018' ),
	'description'           => __( 'Instagram posts pulled in from Instagram.', 'thebirdthebear2018' ),
	'labels'                => $labels,
	'supports'              => array( 'title', 'thumbnail', 'editor' ),
	'taxonomies'            => array( 'category' ),
	'hierarchical'          => false,
	'public'                => true,
	'show_ui'               => true,
	'show_in_menu'          => true,
	'menu_position'         => 5,
	'menu_icon'             => 'data:image/svg+xml;base64,' . base64_encode('<svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill="black" d="M12.67,10A2.67,2.67,0,1,0,10,12.67,2.68,2.68,0,0,0,12.67,10Zm1.43,0A4.1,4.1,0,1,1,10,5.9,4.09,4.09,0,0,1,14.1,10Zm1.13-4.27a1,1,0,1,1-1-1A1,1,0,0,1,15.23,5.73ZM10,3.44c-1.17,0-3.67-.1-4.72.32A2.67,2.67,0,0,0,3.76,5.28c-.42,1-.32,3.55-.32,4.72s-.1,3.67.32,4.72a2.74,2.74,0,0,0,1.52,1.52c1,.42,3.55.32,4.72.32s3.67.1,4.72-.32a2.83,2.83,0,0,0,1.52-1.52c.42-1.05.32-3.55.32-4.72s.1-3.67-.32-4.72a2.74,2.74,0,0,0-1.52-1.52C13.67,3.34,11.17,3.44,10,3.44ZM18,10c0,1.1,0,2.2-.05,3.3a4.84,4.84,0,0,1-1.29,3.36A4.8,4.8,0,0,1,13.3,18C12.2,18,11.1,18,10,18S7.8,18,6.7,18a4.84,4.84,0,0,1-3.36-1.29A4.84,4.84,0,0,1,2.05,13.3C2,12.2,2,11.1,2,10s0-2.2,0-3.3A4.84,4.84,0,0,1,3.34,3.34,4.8,4.8,0,0,1,6.7,2.05C7.8,2,8.9,2,10,2s2.2,0,3.3,0a4.84,4.84,0,0,1,3.36,1.29A4.8,4.8,0,0,1,18,6.7C18,7.8,18,8.9,18,10Z"/></svg>'),
	'show_in_admin_bar'     => true,
	'show_in_nav_menus'     => false,
	'can_export'            => true,
	'has_archive'           => false,
	'exclude_from_search'   => true,
	'publicly_queryable'    => true,
	'capability_type'       => 'post',
);

register_post_type( 'instagram', $args );




// Register Featured Medals

	$labels = array(
		'name'                  => _x( 'Featured Medals', 'Post Type General Name', 'thebirdthebear2018' ),
		'singular_name'         => _x( 'Featured Medal', 'Post Type Singular Name', 'thebirdthebear2018' ),
		'menu_name'             => __( 'Featured Medals', 'thebirdthebear2018' ),
		'name_admin_bar'        => __( 'Featured Medal', 'thebirdthebear2018' ),
		'archives'              => __( 'Featured Medals Archives', 'thebirdthebear2018' ),
		'attributes'            => __( 'Featured Medal Attributes', 'thebirdthebear2018' ),
		'parent_item_colon'     => __( 'Parent Featured Medal:', 'thebirdthebear2018' ),
		'all_items'             => __( 'All Featured Medals', 'thebirdthebear2018' ),
		'add_new_item'          => __( 'Add New Featured Medal', 'thebirdthebear2018' ),
		'add_new'               => __( 'Add New', 'thebirdthebear2018' ),
		'new_item'              => __( 'New Featured Medal', 'thebirdthebear2018' ),
		'edit_item'             => __( 'Edit Featured Medal', 'thebirdthebear2018' ),
		'update_item'           => __( 'Update Featured Medal', 'thebirdthebear2018' ),
		'view_item'             => __( 'View Featured Medal', 'thebirdthebear2018' ),
		'view_items'            => __( 'View Featured Medals', 'thebirdthebear2018' ),
		'search_items'          => __( 'Search Featured Medal', 'thebirdthebear2018' ),
		'not_found'             => __( 'Not found', 'thebirdthebear2018' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'thebirdthebear2018' ),
		'featured_image'        => __( 'Featured Image', 'thebirdthebear2018' ),
		'set_featured_image'    => __( 'Set featured image', 'thebirdthebear2018' ),
		'remove_featured_image' => __( 'Remove featured image', 'thebirdthebear2018' ),
		'use_featured_image'    => __( 'Use as featured image', 'thebirdthebear2018' ),
		'insert_into_item'      => __( 'Insert into Featured Medal', 'thebirdthebear2018' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Featured Medal', 'thebirdthebear2018' ),
		'items_list'            => __( 'Featured Medals list', 'thebirdthebear2018' ),
		'items_list_navigation' => __( 'Featured Medals list navigation', 'thebirdthebear2018' ),
		'filter_items_list'     => __( 'Filter Featured Medals list', 'thebirdthebear2018' ),
    );
    
	$args = array(
		'label'                 => __( 'Featured Medal', 'thebirdthebear2018' ),
		'description'           => __( 'Add logos for the websites you\'ve been featured on.', 'thebirdthebear2018' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail' ),
		'taxonomies'            => array( 'category' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-awards',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => false,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
    );
    
	register_post_type( 'featured-medals', $args );





// Register FAQ

$labels = array(
	'name'                  => _x( 'FAQs', 'Post Type General Name', 'thebirdthebear2018' ),
	'singular_name'         => _x( 'FAQ', 'Post Type Singular Name', 'thebirdthebear2018' ),
	'menu_name'             => __( 'FAQs', 'thebirdthebear2018' ),
	'name_admin_bar'        => __( 'FAQ', 'thebirdthebear2018' ),
	'archives'              => __( 'FAQs Archives', 'thebirdthebear2018' ),
	'attributes'            => __( 'FAQ Attributes', 'thebirdthebear2018' ),
	'parent_item_colon'     => __( 'Parent FAQ:', 'thebirdthebear2018' ),
	'all_items'             => __( 'All FAQs', 'thebirdthebear2018' ),
	'add_new_item'          => __( 'Add New FAQ', 'thebirdthebear2018' ),
	'add_new'               => __( 'Add New', 'thebirdthebear2018' ),
	'new_item'              => __( 'New FAQ', 'thebirdthebear2018' ),
	'edit_item'             => __( 'Edit FAQ', 'thebirdthebear2018' ),
	'update_item'           => __( 'Update FAQ', 'thebirdthebear2018' ),
	'view_item'             => __( 'View FAQ', 'thebirdthebear2018' ),
	'view_items'            => __( 'View FAQs', 'thebirdthebear2018' ),
	'search_items'          => __( 'Search FAQ', 'thebirdthebear2018' ),
	'not_found'             => __( 'Not found', 'thebirdthebear2018' ),
	'not_found_in_trash'    => __( 'Not found in Trash', 'thebirdthebear2018' ),
	'featured_image'        => __( 'Featured Image', 'thebirdthebear2018' ),
	'set_featured_image'    => __( 'Set featured image', 'thebirdthebear2018' ),
	'remove_featured_image' => __( 'Remove featured image', 'thebirdthebear2018' ),
	'use_featured_image'    => __( 'Use as featured image', 'thebirdthebear2018' ),
	'insert_into_item'      => __( 'Insert into FAQ', 'thebirdthebear2018' ),
	'uploaded_to_this_item' => __( 'Uploaded to this FAQ', 'thebirdthebear2018' ),
	'items_list'            => __( 'FAQs list', 'thebirdthebear2018' ),
	'items_list_navigation' => __( 'FAQs list navigation', 'thebirdthebear2018' ),
	'filter_items_list'     => __( 'Filter FAQs list', 'thebirdthebear2018' ),
);

$args = array(
	'label'                 => __( 'FAQs', 'thebirdthebear2018' ),
	'labels'                => $labels,
	'supports'              => array( 'title', 'editor' ),
	'taxonomies'            => array( 'category' ),
	'hierarchical'          => false,
	'public'                => true,
	'show_ui'               => true,
	'show_in_menu'          => true,
	'menu_position'         => 7,
	'menu_icon'             => 'dashicons-format-chat',
	'show_in_admin_bar'     => true,
	'show_in_nav_menus'     => false,
	'can_export'            => true,
	'has_archive'           => false,
	'exclude_from_search'   => true,
	'publicly_queryable'    => true,
	'capability_type'       => 'post',
);

register_post_type( 'qa_faqs', $args );


}

add_action( 'init', 'tbtb_custom_post_type', 0 );

