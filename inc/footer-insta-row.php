<div class="insta-row">
	<?php 
	// the query
	$the_query = new WP_Query( array(
			'post_type' => 'instagram',
			'posts_per_page' => '15'
	) ); ?>

	<?php if ( $the_query->have_posts() ) : ?>

		<!-- pagination here -->

		<!-- the loop -->
		<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
			<?php the_post_thumbnail( 'thumbnail' ); ?>
		<?php endwhile; ?>
		<!-- end of the loop -->

		<!-- pagination here -->

		<?php wp_reset_postdata(); ?>

	<?php else : ?>
		<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
	<?php endif; ?>
</div>