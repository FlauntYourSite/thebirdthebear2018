<div class="blog-thumb-hover">
    <div class="thumb-info">
        <?php if ( 'instagram' == get_post_type() ){ ?>

            <h2>
                <span class="title">Instagram Entry:</span><br />
                <span class="subtitle instagram-subtitle">
                    <!-- Content supplied by javascript -->              
                <span>
            </h2>

        <?php }else{ ?>

            <h2>
                <span class="title">Journal Entry:</span><br />
                <span class="subtitle post-subtitle"><?php couple(); ?><span>
            </h2>
            <?php } ?>

        <p class="thumb-date"><?php the_date( 'F j, Y' ); ?></p>
    </div>
</div>