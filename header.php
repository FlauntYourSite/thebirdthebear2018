<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package TheBirdTheBear2018
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
	<script>
		var controller = new ScrollMagic.Controller();
	</script>
		<link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
</head>

<body <?php body_class(); ?>>

<?php require_once get_template_directory() . '/inc/svg-sprite.svg'; ?>
<img src="<?php echo get_template_directory_uri() . '/images/frame.svg'; ?>" style="display:none" />

<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'thebirdthebear2018' ); ?></a>

	<header id="masthead" class="site-header">
		<?php get_template_part( 'social' ); ?>

		<a class="alt-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
			<?php require get_template_directory() . '/images/bird-bear-logo-small.svg'; ?>
		</a>

		<a class="ribbon" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
			<?php require_once get_template_directory() . '/images/tbtb-ribbon-logo.svg'; ?>
		</a>

		<nav id="site-navigation" class="main-navigation">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Menu', 'thebirdthebear2018' ); ?></button>
			<?php
				wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				) );
			?>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->
	<div id="content" class="site-content">