
/**
 * Animates the fadein of the login logo, and initiates the Featured Image "Make it Rain function" 
 * 
 */

function logoFade() {
  var logo = document.getElementsByTagName("h1")[0];
  // var logoBackground = document.getElementById("login").getElementsByTagName("a")[0];
  // var contain = logoBackground.style.cssText = "background-size:contain!important; ";
  var zIndex = document.getElementById('login').style.cssText = "position:relative; z-index:100; background:none;";

  TweenMax.fromTo(logo, 1, { opacity: 0 }, { opacity: 1, onComplete: makeItRain }, 1);
}

window.onload = logoFade();



/**
 * Calls to the REST API to get the Featured Images.
 * 
 */

var ourRequest = new XMLHttpRequest();
ourRequest.open('GET', tbbt_custom_login_ajaxify.currentSite + '/wp-json/wp/v2/posts');
ourRequest.onload = function () {
  if (ourRequest.status >= 200 && ourRequest.status < 400) {
    var featuredImageData = JSON.parse(ourRequest.responseText);
    makeItRain(featuredImageData);
  } else {
    console.log("We connected to the server, but it returned an error.");
  }
};

ourRequest.onerror = function () {
  console.log("Connection error");
};

ourRequest.send();


/**
 * Make It Rain function controls the animation of the featured images. 
 * 
 */

function makeItRain(featuredImageData) {

  for (var i = 0; i < featuredImageData.length; i++) {

    var rain = document.createElement("div");

    rain.style.cssText = "width:250px; height: 150px; position:absolute; top:-30vh; opacity:0; background-repeat: no-repeat; background-position-x: center; background-size: contain;";
    rain.style.setProperty('left', Math.floor(Math.random() * screen.width) + 'px');
    rain.style.setProperty('background-image', 'url(" ' + featuredImageData[i].featured_image_src + ' ")');

    rainStagger = document.body.appendChild(rain);


    var tl = new TimelineMax({ repeat: -1 });
    tl
      .to(rainStagger, Math.floor(Math.random() * ((13 - 8) + 1) + 8), { transform: 'translateY(130vh)', ease: Linear.easeNone }, Math.random() * 12)
      .to(rainStagger, 0.5, { opacity: 1 }, 0)
      .to(rainStagger, 1, { opacity: 0 });

  }

}