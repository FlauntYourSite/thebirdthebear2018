<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package TheBirdTheBear2018
 */

get_header(); ?>
	<div class="para-hero"><img src="<?php the_field( 'header_image' ); ?>"></div>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<header class="entry-header">
				<?php get_template_part( 'inc/animated-title' ); ?>
			</header><!-- .entry-header -->

			<div class="content-block" id="faq">

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<?php 
					// the query
					$args = array(
						'post_type' => array( 'qa_faqs' )
					);

					$the_query = new WP_Query( $args ); ?>
					
					<?php if ( $the_query->have_posts() ) : ?>
					
						<!-- pagination here -->
						<dl>
							<!-- the loop -->
							<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
								<div class="qa">
									<a href="#/">
										<div class="question-x"><?php require( 'images/x-hover.svg' ); ?></div>
										<dt class="question"><?php the_title(); ?></dt>
									</a>
									<dd class="answer"><?php the_content(); ?></dd>

									<script type="application/ld+json">
										{
											"@context": "http://schema.org",
											"@type": "Question",
											"text": "<?php the_title(); ?>",
											"acceptedAnswer": {
												"@type": "Answer",
												"text": "<?php echo wp_strip_all_tags( get_the_content() ); ?>"
											}
										}
									</script>
								</div>
							<?php endwhile; ?>
							<!-- end of the loop -->
						</dl>
						<!-- pagination here -->
					
						<?php wp_reset_postdata(); ?>
					
					<?php else : ?>
						<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
					<?php endif; ?>
				</article>

			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
