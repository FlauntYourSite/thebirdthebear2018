<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package TheBirdTheBear2018
 */

get_header(); ?>
	<?php tbtb_slideshows( 'tbtb_header_slideshow', 'swiper-header services' ); ?>
	
	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) : the_post(); ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<header class="entry-header">
					<?php get_template_part( 'inc/animated-title' ); ?>
			</header><!-- .entry-header -->

	
			<section class="row fade">
					<div class="content-block services-left">
						<div>
							<h2>
								<span class="title"><?php the_field( 'services_section_1_title' ); ?></span><br />
								<span class="subtitle"><?php the_field( 'services_section_1_subtitle' ); ?></span>
							</h2>
							<?php the_field( 'services_section_1_text' ); ?>
						</div>
					</div>
					<?php include( 'images/four-branch.svg' ); ?>
					<?php include( 'images/four-branch.svg' ); ?>

					<div class='polaroid polaroid-right'>
						<?php 
							if ( get_field( 'section_1_photo_video_selector' ) == 'Photo Gallery' ){
								tbtb_slideshows( 'services_section_1_slideshow', '' );
							} elseif ( get_field( 'section_1_photo_video_selector' ) == 'Video' ){
								video_container( 'section_1_video'); 
							}
						?>
					</div>
			</section>



	
			<section class="row fade">
				<div class="content-block services-right">
					<div>
						<h2>
							<span class="title"><?php the_field( 'services_section_2_title' ); ?></span><br />
							<span class="subtitle"><?php the_field( 'services_section_2_subtitle' ); ?></span>
						</h2>
						<?php the_field( 'services_section_2_text' ); ?>
					</div>
				</div>
				<div class='polaroid polaroid-left'>
					<?php 
						if ( get_field( 'section_2_photo_video_selector' ) == 'Photo Gallery' ){
							tbtb_slideshows( 'services_section_2_slideshow', '' );
						} elseif ( get_field( 'section_2_photo_video_selector' ) == 'Video' ){
							video_container( 'section_2_video'); 
						}
					?>
				</div>
				<div class="frilly-circle"><?php include( 'images/frilly-circle.svg' ); ?></div>
			</section>


				
			<section class="row fade">
				<div class="content-block services-left">
					<h2>
						<span class="title"><?php the_field( 'services_section_3_title' ); ?></span><br />
						<span class="subtitle"><?php the_field( 'services_section_3_subtitle' ); ?></span>
					</h2>
					<?php the_field( 'services_section_3_text' ); ?>
				</div>
				<div class='polaroid polaroid-right'>
					<?php 
						if ( get_field( 'section_3_photo_video_selector' ) == 'Photo Gallery' ){
							tbtb_slideshows( 'services_section_3_slideshow', '' );
						} elseif ( get_field( 'section_3_photo_video_selector' ) == 'Video' ){
							video_container( 'section_3_video' ); 
						}
					?>
				</div>
				<?php include( 'images/four-branch.svg' ); ?>
				<?php include( 'images/four-branch.svg' ); ?>
			</section>

		<?php endwhile; ?>

		<div class="usa"><?php require( 'images/usa.svg' ); ?></div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
